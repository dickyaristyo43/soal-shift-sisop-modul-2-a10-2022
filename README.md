# soal-shift-sisop-modul-2-A10-2022

Praktikum Sistem Operasi (A)

- Ferdinand Putra Gumilang Silalahi - 5025201176
- Elbert Dicky Aristyo - 5025201231
- Alif Adrian Anzary - 5025201274



Praktikum Sistem Operasi (A)
Ferdinand Putra Gumilang Silalahi - 5025201176
Elbert Dicky Aristyo - 5025201231
Alif Adrian Anzary - 5025201274
Penjelasan Revisi no. 3 :
Kode kami diawali dengan melakukan spawing process. setelah melakukan spawning process tersebut, pada child process kami menggunakan perintah unzip untuk melakukan unzip pada files animal.zip.
char *argv[] = {"mkdir", "-p", folderName, NULL};
        execv("/bin/mkdir", argv);


Penjelasan no.1 :

Oleh karena soal meminta program untuk bekerja pada waktu tertentu, agar program dapat siap pada waktu tertentu tersebut, kita akan membuat suatu daemon process yang dapat tetap berjalan di background.

~~~
int main() {
  pid_t pid, sid;        

  pid = fork();    

  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/home/qerqewr/Modul 2")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);


~~~

1a. Saat program pertama kali berjalan, akan di download file characters dan file weapons dari link google drive, lalu program akan mengekstrak kedua file tersebut. Hal tersebut akan dilakukan dengan fungsi downloadUnzip, dimana child akan difork untuk menjalankan wget, dan parent akan jalankan unzip.

~~~
void downloadUnzip(char *name, char *link, char *dir) {
    pid_t child_id;
    int status;
    
    child_id = fork();

		if (child_id < 0) {
    exit(EXIT_FAILURE); 
    }
    
    if (child_id == 0) {
        char *argv[] = {"wget", link, NULL};       
        execv("/bin/wget", argv);
        
    } else {
        while ((wait(&status)) > 0);
        char *argv2[] = {"unzip", name, "-d", dir};
				execv("/bin/unzip", argv2);
    }
}
~~~
Kemudian dibuat folder/directory dengan nama “gacha_gacha” dengan mkdir. Apabila folder sudah ada maka akan dilewati.

~~~
void createDir(char *name) {
    
   	DIR *dp = opendir(name); 
    if (dp) {
        return;
    }

    pid_t child_id;
    int status;
    
    child_id = fork();
 
    if (child_id < 0) {
    exit(EXIT_FAILURE); 
    }
    
    if (child_id == 0) {
        char *argv3[] = {"mkdir", "-p", name, NULL};
        execv("/bin/mkdir", argv3);
        
    } else {
        while ((wait(&status)) > 0);
        return;
    }
}
~~~
1.d untuk pengambilan nama, rarity, dan tipe item yang berada didalam file tipe json, digunakan library <json-c/json.h> untuk dapat membaca dan mengambil konten dalam file json.

~~~
void get_json_value(char *filename, char *name_val, char *rarity_val)
{
    FILE *fp;
    char buffer[4095];


    struct json_object *parsed_json;
    struct json_object *rarity;
    struct json_object *name;

    fp = fopen(filename,"r");
    fread(buffer, 4095, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json,"rarity",&rarity);
    json_object_object_get_ex(parsed_json,"name",&name);

    sprintf(name_val,"%s",json_object_get_string(name));
    sprintf(rarity_val,"%s",json_object_get_string(rarity));
}
~~~


1e. Untuk mengacha item pada saat 30 maret pada jam 04:44, akan digunakan fungsi time(NULL) agar dapat return waktu pada sistem, yang kemudian dibandingkan dengan waktu 30 maret 04:44.

~~~
 while (1) {
//akan dijalankan saat 30 maret 2022 04:44 dan tetap berjalan sampai 3 jam
if(time(NULL) >= 1648565040){
				
        if (curr_primogems < 0) continue;

        if((jumlahGacha%90)==0) createDir();
        
        if((jumlahGacha%10)==0)
        
				primogems -= 160;
				jumlahGacha++;

//akan dijalankan saat 30 maret 2022 07:44
}else if(time(NULL) = 1648575840){
char *argv4[] = {"zip", "-rq", "--password", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
execv("/bin/zip", argv4);
break;
}
    sleep(1);
  }
  return 0;
}

~~~
Disini, sistem akan mengulang loop setiap 1 detik, dan akan mengecek waktu sistem. Jika waktu berjalan pas 3 jam setelah 30 maret 04:44, maka file gacha_gacha yang berisi hasil gacha akan dizip dan diberi password satuduatiga.

Permasalahan soal 1 :
Masalah yang dihadapi saat mengerjakan soal 2 adalah :
- Nama zip belum dapat diubah, sehingga argv unzip tidak dapat dijalankan karena nama zip berbeda 
<a href="https://ibb.co/Bzhq10x"><img src="https://i.ibb.co/FxQgPRM/demosoal1.png" alt="demosoal1" border="0"></a>
- Untuk 1b dan 1c, kita masih kesulitan untuk mengimplementasikan fungsi untuk membaca, menamakan, dan menginput didalam file txt.




Penjelasan Revisi no 3 :

3.a Membuat 2 directory hewan air dan darat

~~~
createFolder(locdir, "darat");
	sleep(3);
	createFolder(locdir, "air");
	unzip(zipdir, locdir);
~~~

3.b Mengextract "animal.zip"
~~~
char *argv[] = {"unzip", "-j", zipdir, "animal/*", "-d", locdir, NULL};
		execv("/bin/unzip", argv);
~~~

3.c Setelah extract dipisah, untuk hewan darat di masukan ke dalam folder darat dan hewan air di masukan ke dalam folder air. untuk hewan yang tidak memiliki keterangan air atau darat harus di hapus
~~~
char *argv[] = {"mv", srcFileDir, destFileDir, NULL};
		execv("/bin/mv", argv);
~~~

3.d Menghapus semua burung yang ada pada directory hewan darat. Hewan burung di tandai dengan "bird" pada nama file
~~~
closedir(dp);

	char daratDir[100];
	strcpy(daratDir, locdir);
	strcat(daratDir, "/darat");
	dp = opendir(daratDir);
	if (dp != NULL){
		while ((ep = readdir(dp)) != NULL){
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
				if (strstr(ep->d_name, "bird") != NULL){
					deletePhoto(ep->d_name, daratDir);
				}
			}
		}
	}
~~~

3.e Meminta membuat list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut
~~~
closedir(dp);

	char airDir[100];
	strcpy(airDir, locdir);
	strcat(airDir, "/air");
	dp = opendir(airDir);
	if (dp != NULL){
		while ((ep = readdir(dp)) != NULL){
			struct stat info;
			int r;
			if (strcmp(ep->d_name, ".") != 0 && strcmp(ep->d_name, "..") != 0){
				char listFile[100];
				strcpy(listFile, airDir);
				strcat(listFile, "/list.txt");

				char pathFile[100];
				strcpy(pathFile, airDir);
				strcat(pathFile, "/");
				strcat(pathFile, ep->d_name);
				
				FILE *fp = fopen(listFile, "a");
				r = stat(pathFile, &info);

				fprintf(fp, "%s_", getpwuid(info.st_uid)->pw_name);

				if (info.st_mode & S_IRUSR && info.st_mode & S_IWUSR && info.st_mode & S_IXUSR)
					fprintf(fp, "rwx");
				else if (info.st_mode & S_IRUSR && info.st_mode & S_IWUSR)
					fprintf(fp, "rw");
				else if (info.st_mode & S_IRUSR && info.st_mode & S_IXUSR)
					fprintf(fp, "rx");
				else if (info.st_mode & S_IRUSR)
					fprintf(fp, "r");
				else if (info.st_mode & S_IWUSR && info.st_mode & S_IXUSR)
					fprintf(fp, "wx");
				else if (info.st_mode & S_IWUSR)
					fprintf(fp, "w");
				else if (info.st_mode & S_IXUSR)
					fprintf(fp, "x");

				char *name = strtok(ep->d_name, "_");
				fprintf(fp, "_%s\n", name);
				fclose(fp);
			}
		}
	}
~~~

1. fungsi createFolder
~~~
void createFolder(char *locdir, char *category){
	char folderName[100];
	strcpy(folderName, locdir);
	strcat(folderName, "/");
	strcat(folderName, category);

	printf("%s\n", folderName);

	pid_t child_id;
	int status;
	child_id = fork();

	if (child_id < 0)
	{
		exit(EXIT_FAILURE);
	}

	if (child_id == 0)
	{
		char *argv[] = {"mkdir", "-p", folderName, NULL};
		execv("/bin/mkdir", argv);
	}
	else
	{
		while ((wait(&status)) > 0)
			;
		return;
	}
}
~~~
pada fungsi ini kami membuat sebuah child process yang didalamnya akan membuat sebuah folder baru berdasarkan parameter yang diterima di dalam folder animal. perintah yang kami gunakan adalah mkdir. tag "-p" digunakan untuk menghiraukan error jika file yang akan dibentuk sudah ada sebelumnya.

2. fungsi movePhoto
~~~
void movePhoto(char *fileName, char *category, char *locdir){
	char srcFileDir[100];
	strcpy(srcFileDir, locdir);
	strcat(srcFileDir, "/");
	strcat(srcFileDir, fileName);

	char destFileDir[100];
	strcpy(destFileDir, locdir);
	strcat(destFileDir, "/");
	strcat(destFileDir, category);
	strcat(destFileDir, "/");
	strcat(destFileDir, fileName);

	pid_t child_id;
	child_id = fork();
	int status;

	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	if (child_id == 0){
		char *argv[] = {"mv", srcFileDir, destFileDir, NULL};
		execv("/bin/mv", argv);
	}
	else{
		while ((wait(&status)) > 0);
		return;
	}
}
~~~
pada fungsi ini kami membuat child process yang berfungsi untuk memindahkan foto ke folder hewan air atau darat.

3. fungsi deletePhoto
~~~
void deletePhoto(char *fileName, char *locdir){
	char fileDir[100];
	strcpy(fileDir, locdir);
	strcat(fileDir, "/");
	strcat(fileDir, fileName);

	pid_t child_id;
	child_id = fork();
	int status;

	if (child_id < 0){
		exit(EXIT_FAILURE);
	}
	if (child_id == 0){
		char *argv[] = {"rm", fileDir, NULL};
		execv("/bin/rm", argv);
	}
	else{
		while ((wait(&status)) > 0)
			;
		return;
	}
}
~~~
soal meminta untuk menghapus foto untuk hewan yang tidak ada keterangan air atau darat. kami menggunakan perintah "rm" untuk menghapus foto tersebut. foto yang telah di hapus tidak akan masuk ke dalam folder hewan air atau darat.

Hasil run program:
![Hasil Run 1](https://i.ibb.co/wS8nfWb/hasil-run-1.png)
![Hasil Run 2](https://i.ibb.co/2nZPDP6/hasil-run-2.png)
