#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <wait.h>
#include <json-c/json.h>


void downloadUnzip(char *name, char *link, char *dir) {
    pid_t child_id;
    int status;
    
    child_id = fork();

		if (child_id < 0) {
    exit(EXIT_FAILURE); 
    }
    
    if (child_id == 0) {
        char *argv[] = {"wget", link, NULL};       
        execv("/bin/wget", argv);
        
    } else {
        while ((wait(&status)) > 0);
        char *argv2[] = {"unzip", name, "-d", dir};
				execv("/bin/unzip", argv2);
    }
}

void createDir(char *name) {
    
   	DIR *dp = opendir(name); 
    if (dp) {
        return;
    }

    pid_t child_id;
    
    child_id = fork();
 
    if (child_id < 0) {
    exit(EXIT_FAILURE); 
    }
    
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", name, NULL};
        execv("/bin/mkdir", argv);
        
    } else {
        wait(NULL);
    }
}

void get_json_value(char *filename, char *name_val, char *rarity_val)
{
    FILE *fp;
    char buffer[4095];


    struct json_object *parsed_json;
    struct json_object *rarity;
    struct json_object *name;

    fp = fopen(filename,"r");
    fread(buffer, 4095, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json,"rarity",&rarity);
    json_object_object_get_ex(parsed_json,"name",&name);

    sprintf(name_val,"%s",json_object_get_string(name));
    sprintf(rarity_val,"%s",json_object_get_string(rarity));
}







int main() {
  pid_t pid, sid;        

  pid = fork();    

  if (pid < 0) {
    exit(EXIT_FAILURE);
  }

  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  umask(0);

  sid = setsid();
  if (sid < 0) {
    exit(EXIT_FAILURE);
  }

  if ((chdir("/home/qerqewr/Modul 2")) < 0) {
    exit(EXIT_FAILURE);
  }

  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

//pertama, program download file character dari link, kemudian diekstrak ke folder database,

downloadUnzip ("Anggap_ini_database_weapon.zip", "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download", "database_weapon");
downloadUnzip ("Anggap_ini_database_characters.zip", "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download", "database_character");

//kemudian buat folder gacha_gacha

createDir ("gacha_gacha");

int primogems = 79000;

  while (1) {
//akan dijalankan saat 30 maret 2022 04:44 dan tetap berjalan sampai 3 jam
if(time(NULL) >= 1648565040){
				
        if (curr_primogems < 0) continue;

        if((jumlahGacha%90)==0) createDir();
        
        if((jumlahGacha%10)==0)
        
				primogems -= 160;
				jumlahGacha++;

//akan dijalankan saat 30 maret 2022 07:44
}else if(time(NULL) = 1648575840){
char *argv4[] = {"zip", "-rq", "--password", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", NULL};
execv("/bin/zip", argv4);
break;
}
    sleep(1);
  }
  return 0;
}

